output "zones" {
  value = data.aws_availability_zones.zones.names
}

output "route-table" {
  value = aws_subnet.stage_public[*].id
}