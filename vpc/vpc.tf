data "aws_availability_zones" "zones" {
  state = "available"
}

//VPC
resource "aws_vpc" "stage_vpc" {
  cidr_block         = "192.0.0.0/16"
  enable_dns_support = true

  tags = {
    Name      = "stage-vpc",
    Terraform = "true"
  }
}

//Internet-Gateway

resource "aws_internet_gateway" "stage_igw" {
  vpc_id = aws_vpc.stage_vpc.id

  tags = {
    Name = "stage-igw"
  }
}

//subnet
resource "aws_subnet" "stage_public" {
  vpc_id                  = aws_vpc.stage_vpc.id
  map_public_ip_on_launch = true
  count                   = length(data.aws_availability_zones.zones.names)
  availability_zone       = element(data.aws_availability_zones.zones.names, count.index)
  cidr_block              = element(var.public_subnet, count.index)

  tags = {
    Name = "stage-public-${count.index + 1}-subnet"
  }
}

resource "aws_subnet" "stage_private" {
  vpc_id            = aws_vpc.stage_vpc.id
  count             = length(data.aws_availability_zones.zones.names)
  availability_zone = element(data.aws_availability_zones.zones.names, count.index)
  cidr_block        = element(var.private_subnet, count.index)

  tags = {
    Name = "stage-private-${count.index + 1}-subnet"
  }
}

resource "aws_subnet" "stage_data" {
  vpc_id            = aws_vpc.stage_vpc.id
  count             = length(data.aws_availability_zones.zones.names)
  availability_zone = element(data.aws_availability_zones.zones.names, count.index)
  cidr_block        = element(var.data_subnet, count.index)

  tags = {
    Name = "stage-data-${count.index + 1}-subnet"
  }
}

//EIP
resource "aws_eip" "stage_eip" {
  vpc = true

  tags = {
    Name = "stage-eip"
  }
}

//NAT Gateway
resource "aws_nat_gateway" "stage_nat" {
  allocation_id = aws_eip.stage_eip.id
  subnet_id     = aws_subnet.stage_public[1].id

  tags = {
    Name = "stage-natgw"
  }

}

//Route tables
resource "aws_route_table" "stage_public" {
  vpc_id = aws_vpc.stage_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.stage_igw.id
  }

  tags = {
    Name = "stage-public"
  }
}

resource "aws_route_table" "stage_private" {
  vpc_id = aws_vpc.stage_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.stage_nat.id
  }

  tags = {
    Name = "stage-private"
  }
}

//association

resource "aws_route_table_association" "pub_association" {
  count          = length(aws_subnet.stage_public[*].id)
  subnet_id      = element(aws_subnet.stage_public[*].id, count.index)
  route_table_id = aws_route_table.stage_public.id
}

resource "aws_route_table_association" "private_association" {
  count          = length(aws_subnet.stage_public[*].id)
  subnet_id      = element(aws_subnet.stage_private[*].id, count.index)
  route_table_id = aws_route_table.stage_private.id
}

resource "aws_route_table_association" "data_association" {
  count          = length(aws_subnet.stage_public[*].id)
  subnet_id      = element(aws_subnet.stage_data[*].id, count.index)
  route_table_id = aws_route_table.stage_private.id
}