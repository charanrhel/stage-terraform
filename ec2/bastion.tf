
data "external" "myipaddr" {
  program = ["bash", "-c", "curl -s 'https://ipinfo.io/json'"]
}

data "aws_vpc" "vpc_list" {

  filter {
    name   = "tag:Name"
    values = ["stage-vpc"]
  }
}

data "aws_subnet" "pub_subnet_list" {
  vpc_id = data.aws_vpc.vpc_list.id

  filter {
    name   = "tag:Name"
    values = ["stage-public-1-subnet"]
  }
}

//Security Group
resource "aws_security_group" "stage_bastion" {
  name        = "stage_bastion_sg"
  description = "Allows Admins to Bastion"
  vpc_id      = data.aws_vpc.vpc_list.id

  ingress {
    description = "Allow Admins for SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.external.myipaddr.result.ip}/32"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "stage-bastion-sg"
  }
}

//EC2 Instance
resource "aws_instance" "stage_bastion" {
  ami             = "ami-0ceecbb0f30a902a6"
  instance_type   = "t2.micro"
  subnet_id       = data.aws_subnet.pub_subnet_list.id
  vpc_security_group_ids = [aws_security_group.stage_bastion.id]

  tags = {
    Name = "stage-bastion"
  }
}

