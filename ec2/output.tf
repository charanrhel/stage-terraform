output "vpc_list" {
  value = [data.aws_vpc.vpc_list.id]

}
output "my_ip" {
  value = data.external.myipaddr.result.ip
}
output "bastion-sg" {
  value = [aws_security_group.stage_bastion]
}